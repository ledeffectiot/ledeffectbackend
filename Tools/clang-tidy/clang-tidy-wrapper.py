#!/usr/bin/env python3
# This is a wrapper script that converts a GCC compilation command for a Zynq
# target to a Clang-compatible command on ARM.
import os.path
import subprocess
import sys

args = list(sys.argv)

if len(args) < 2 or not args[1].endswith("clang-tidy"):
    print(
        "error: clang-tidy-on-arm.py: "
        "expecting the first argument to be a path to Clang Tidy executable."
    )
    sys.exit(1)

clang_tidy = args[1]
if not os.path.exists(clang_tidy):
    print(
        "error: clang-tidy-on-arm.py: "
        f"the path to Clang Tidy does not exist: {clang_tidy}."
    )
    sys.exit(1)

# Remove the first argument (which is the path to this script) because it is not
# to be passed to the underlying Clang Tidy invocation.
_ = args.pop(0)

# Clang does not support this argument: -march=armv7-a.
# error: unknown target CPU 'arm7v-a'
# So we replace it with a generic ARM target.
# See clang -print-targets for available targets.
try:
    march_idx = args.index('-march=armv7-a')
    if(march_idx>0):
        args[march_idx] = "--target=arm-none-eabi"

    march_idx = args.index('--specs=nano.specs')
    if(march_idx>0):
        args[march_idx] = ""

    march_idx = args.index('--specs=nosys.specs')
    if(march_idx>0):
        args[march_idx] = ""
except Exception as e:
    pass

try:
    march_idx = args.index('--specs=nano.specs')
    if(march_idx>0):
        args[march_idx] = ""
except Exception as e:
    print("NO nano specs")
    pass



# Clang Tidy needs to access stdio.h and cpuset.h
# This include path seems to be substituted by RTEMS Toolchain automatically,
# but this script has to do it manually here because we are calling Clang Tidy
# which calls into Clang to parse the AST and the RTEMS Toolchain's GCC is not
# used.
#args.insert(march_idx, "-I/opt/rtems-6-arm-toolchain/arm-rtems6/include")

print(f"clang-tidy-on-arm.py: checking file: {args[-1]}")
result = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
print(result.stdout)
if result.returncode != 0:
    print(result.stderr)
    print(f"clang-tidy-on-arm.py: Clang Tidy has finished with error code: {result.returncode}")
    sys.exit(1)