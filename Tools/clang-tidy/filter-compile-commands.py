#!/usr/bin/env python3
import json
import sys
import os

def filter_compile_commands(input_file, output_file):
    """
    Read the compile_commands.json file, remove unsupported compiler flags,
    and write the result to a new json file.

    Args:
    input_file (str): Path to the input compile_commands.json file.
    output_file (str): Path to the output filtered_compile_commands.json file.
    """
    try:
        with open(input_file, 'r') as file:
            data = json.load(file)
        
        ##os.remove(input_file)
        
        for item in data:
            # Remove the unsupported flags
            item['command'] = item['command'].replace('--specs=nano.specs', '')
            item['command'] = item['command'].replace('--specs=nosys.specs', '')
            
        
        with open(input_file, 'w') as file:
            json.dump(data, file, indent=4)

        with open(output_file, 'w') as file:
            json.dump(data, file, indent=4)

        print(f"Filtered compile commands have been written to {output_file}")
        #os.remove(input_file)

    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python filter_compile_commands.py <input_file> <output_file>")
        sys.exit(1)
    
    input_file_path = sys.argv[1]
    output_file_path = sys.argv[2]
    filter_compile_commands(input_file_path, output_file_path)
