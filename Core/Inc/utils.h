
#ifndef __APP_UTILS_H
#define __APP_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <ctype.h>
#include <stddef.h>
#include <string.h>

    char *trimnwhitespace(char *str, size_t n);

#ifdef __cplusplus
}
#endif

#endif
