#ifndef __RTC_CONFIG_H
#define __RTC_CONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <appCommon.h>

#include "stm32f7xx_hal.h"

    void RTCTask(void *argument);

    TaskHandle_t rtcTaskHandle;

#ifdef __cplusplus
}
#endif

#endif // __RTC_CONFIG_H