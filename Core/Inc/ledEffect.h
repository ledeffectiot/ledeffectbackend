
#ifndef __LED_EFFECT_H
#define __LED_EFFECT_H

#ifdef __cplusplus
extern "C"
{
#endif

// #include <FreeRTOS.h>
// #include <SEGGER_SYSVIEW.h>

// #include <task.h>
#include <appCommon.h>
#include <main.h>
#include <timers.h>

#include "stm32f7xx_hal.h"

#define COMMAND_QUEUE_LENGTH (15)

    /**
     * @brief This struct holds the internal state of the LedEffect task.
     *
     * This structure is meant to be passed for all LedEffect methods.
     */
    typedef struct
    {
        TaskHandle_t ledEffectTaskHandle; /**< The handle of the freeRTOS task that gets created by this class */
        StackType_t ledEffectTaskStack[STACK_SIZE]; /**< Static Stack of the Task */
        StaticTask_t ledEffectTaskTCB;              /**< Static Task control block*/

        // software timer for LED effect timing
        LedEffectCmd led_mode; // currently commanded LED mode
        TimerHandle_t ledTimer;

        // void (*ledTimerCallback)(TimerHandle_t xTimer);

        /**
         * @brief holds a list of commands this task should execute
         *
         * Each command held is of type command_t
         *
         */
        QueueHandle_t commandQueue;

    } LEDEffect;

    /**
     * @brief Initializes the internal state of the LEDEffect Task
     *
     * Users of this class should create an LEDEffect struct that holds state for this instance
     *
     * @param[in out] internal_state Pointer to the LEDEffect state structure. All internal memebers get initialized
     */
    void ledEffectInit(LEDEffect *internal_state);

    BaseType_t isLedEffectInitialized(LEDEffect *);

#ifdef __cplusplus
}
#endif

#endif //__LED_EFFECT_H