function(add_cmake_format_target)
    if(NOT ${ENABLE_CMAKE_FORMAT})
        return()
    endif()
    set(ROOT_CMAKE_FILES "${CMAKE_SOURCE_DIR}/CMakeLists.txt")
    file(GLOB_RECURSE CMAKE_FILES_TXT "*/CMakeLists.txt")
    file(GLOB_RECURSE CMAKE_FILES_C "cmake/*.cmake")
    list(
        FILTER
        CMAKE_FILES_TXT
        EXCLUDE
        REGEX
        "${CMAKE_SOURCE_DIR}/(build|external)/.*")
    set(CMAKE_FILES ${ROOT_CMAKE_FILES} ${CMAKE_FILES_TXT} ${CMAKE_FILES_C})
    find_program(CMAKE_FORMAT cmake-format)
    if(CMAKE_FORMAT)
        message(STATUS "Added Cmake Format")
        set(FORMATTING_COMMANDS)
        foreach(cmake_file ${CMAKE_FILES})
            list(
                APPEND
                FORMATTING_COMMANDS
                COMMAND
                cmake-format
                -c
                ${CMAKE_SOURCE_DIR}/.cmake-format.yaml
                -i
                ${cmake_file})
        endforeach()
        add_custom_target(
            run_cmake_format
            COMMAND ${FORMATTING_COMMANDS}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
    else()
        message(WARNING "CMAKE_FORMAT NOT FOUND")
    endif()
endfunction()

function(add_clang_format_target)
    if(NOT ${ENABLE_CLANG_FORMAT})
        return()
    endif()
    find_package(Python3 COMPONENTS Interpreter)
    if(NOT ${Python_FOUND})
        return()
    endif()
    file(GLOB_RECURSE CMAKE_FILES_CC "*/*.cc")
    file(GLOB_RECURSE CMAKE_FILES_CPP "*/*.cpp")
    file(GLOB_RECURSE CMAKE_FILES_H "*/*.h")
    file(GLOB_RECURSE CMAKE_FILES_HPP "*/*.hpp")
    set(CPP_FILES
        ${CMAKE_FILES_CC}
        ${CMAKE_FILES_CPP}
        ${CMAKE_FILES_H}
        ${CMAKE_FILES_HPP})
    list(
        FILTER
        CPP_FILES
        EXCLUDE
        REGEX
        "${CMAKE_SOURCE_DIR}/(build|Release|Debug|ThirdParty|Drivers|external)/.*")
    find_program(CLANGFORMAT clang-format)
    if(CLANGFORMAT)
        message(STATUS "Added Clang Format")
        add_custom_target(
            run_clang_format
            COMMAND
                ${Python3_EXECUTABLE}
                ${CMAKE_SOURCE_DIR}/tools/run-clang-format.py ${CPP_FILES}
                --in-place
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            USES_TERMINAL)
    else()
        message(WARNING "CLANGFORMAT NOT FOUND")
    endif()
endfunction()


function(add_cppcheck_to_target target)
    set(cppcheck
    cppcheck
    "--enable=warning,performance,portability,style"
    "--std=c17"
    "--library=freertos"
    "--inconclusive"
    "--force" 
    "--inline-suppr"
    "--memleak"
    "--invalidPointer"
    )
    set_target_properties(${target} PROPERTIES C_CPPCHECK ${cppcheck})
endfunction()



function(add_clangtidy_to_target target)
    # Filter compile_commands.json
    set(CLANG_TIDY_COMMAND 
        ${Python3_EXECUTABLE}
        ${CMAKE_SOURCE_DIR}/Tools/clang-tidy/clang-tidy-wrapper.py
        "/usr/bin/clang-tidy"
        "--config-file=${CMAKE_SOURCE_DIR}/Tools/clang-tidy/clang-tidy.yaml"
        "-extra-arg-before=-std=${CMAKE_C_STANDARD}"
        "-extra-arg=--target=arm-none-eabi"
        "-extra-arg=-I${TOOLCHAIN_PATH}/arm-none-eabi/include"
        "-extra-arg=--sysroot=${TOOLCHAIN_PATH}/arm-none-eabi/include"
        "-p=${CMAKE_BINARY_DIR}"
    )
    
    message("CLANG_TIDY_COMMAND:" ${CLANG_TIDY_COMMAND})
    
    set_target_properties(${target} PROPERTIES C_CLANG_TIDY "${CLANG_TIDY_COMMAND}")
endfunction()


# Function to create a clang-format target and run it before building the main target
function(add_clangformat_to_target TARGET_NAME)
    # Find all project files
    file(GLOB_RECURSE ALL_C_SOURCE_FILES 
        ${CMAKE_CURRENT_SOURCE_DIR}/Core/Src/*.c
        ${CMAKE_CURRENT_SOURCE_DIR}/Core/Inc/*.h)

    # Add a custom target for clang-format
    add_custom_target(
        clang-format
        COMMAND clang-format --Werror -i --style=Microsoft --fallback-style=Google --verbose ${ALL_C_SOURCE_FILES}
        COMMENT "Running clang-format on all source files"
    )

    # Ensure the main target depends on this running
    add_dependencies(${TARGET_NAME} clang-format)
endfunction()