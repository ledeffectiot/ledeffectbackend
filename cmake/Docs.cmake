# Check if Doxygen is installed
find_package(Doxygen)


include(FetchContent)
FetchContent_Declare(
    doxygen_awesome_css
    GIT_REPOSITORY https://github.com/jothepro/doxygen-awesome-css.git
    GIT_TAG v2.3.2
    GIT_SHALLOW TRUE)

FetchContent_GetProperties(doxygen_awesome_css)
if(NOT doxygen_awesome_css_POPULATED)
	FetchContent_Populate(doxygen_awesome_css)
endif()

set(DOXYGEN_AWESOME_CSS_PATH "${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome.css")
set(DOXYGEN_AWESOME_CSS_SIDEBAR_PATH "${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome-sidebar-only.css")
set(DOXYGEN_HTML_EXTRA_STYLESHEET "${CMAKE_SOURCE_DIR}/Tools/Doxygen/custom.css  ${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome-sidebar-only-darkmode-toggle.css")
set(DOXYGEN_HTML_HEADER "${CMAKE_SOURCE_DIR}/Tools/Doxygen/header.html")
set(DOXYGEN_HTML_EXTRA_FILES "${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome-darkmode-toggle.js")

if(DOXYGEN_FOUND)
    
    # Set the directories to be excluded
    set(DOXYGEN_EXCLUDE "${CMAKE_SOURCE_DIR}/Test ${CMAKE_SOURCE_DIR}/Drivers ${CMAKE_SOURCE_DIR}/Core/ThirdParty")
    ##set(OUTPUT_DIRECTORY ${})
    set(DOXYGEN_INPUT "${CMAKE_SOURCE_DIR}/Core ${CMAKE_SOURCE_DIR}/Tools/Doxygen/custom ${CMAKE_SOURCE_DIR}/README.md")
    set(DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/Documentation)
    
    # set input and output files
    set(DOXYGEN_IN  ${CMAKE_CURRENT_SOURCE_DIR}/Tools/Doxygen/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Tools/Doxygen/Doxyfile)

    # request to configure the file
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

    # note the option ALL which allows to build the docs together with the application
    add_custom_target(doc ALL
                      COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
                      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                      COMMENT "Generating API documentation with Doxygen"
                      VERBATIM
                      DEPENDS ${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome.css
                                ${doxygen_awesome_css_SOURCE_DIR}/doxygen-awesome-sidebar-only.css)
    
    message("Doxygen Added")
else (DOXYGEN_FOUND)
  message("Doxygen need to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)
